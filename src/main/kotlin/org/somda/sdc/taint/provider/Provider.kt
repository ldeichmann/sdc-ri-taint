package org.somda.sdc.taint.provider

import com.google.common.collect.Iterables
import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.Service
import com.google.inject.Injector
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.PreprocessingException
import org.somda.sdc.biceps.model.extension.ExtensionType
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdc.biceps.model.participant.AbstractMetricValue.MetricQuality
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor.AllowedValue
import org.somda.sdc.biceps.provider.access.LocalMdibAccess
import org.somda.sdc.biceps.provider.access.factory.LocalMdibAccessFactory
import org.somda.sdc.dpws.DpwsFramework
import org.somda.sdc.dpws.DpwsUtil
import org.somda.sdc.dpws.device.DeviceSettings
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsaddressing.model.EndpointReferenceType
import org.somda.sdc.extension.model.codedattribute.CodedAttributesType
import org.somda.sdc.extension.model.codedattribute.CodedStringAttributeType
import org.somda.sdc.extension.model.codedattribute.MdcAttributeType
import org.somda.sdc.glue.common.FallbackInstanceIdentifier
import org.somda.sdc.glue.common.MdibXmlIo
import org.somda.sdc.glue.common.factory.ModificationsBuilderFactory
import org.somda.sdc.glue.provider.SdcDevice
import org.somda.sdc.glue.provider.SdcDevicePlugin
import org.somda.sdc.glue.provider.factory.SdcDeviceFactory
import org.somda.sdc.glue.provider.plugin.SdcRequiredTypesAndScopes
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver
import org.somda.sdc.taint.BaseUtil
import org.somda.sdc.taint.Config
import org.somda.sdc.taint.Handles
import java.io.IOException
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.net.InetAddress
import java.net.NetworkInterface
import java.time.Instant
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream
import kotlin.collections.Iterator
import kotlin.collections.setOf


/**
 * Create an instance of an SDC Provider.
 *
 * @param providerUtil options and configured injector
 */
class Provider(providerUtil: ProviderUtil) : AbstractIdleService() {
    private val injector: Injector
    private val mdibAccess: LocalMdibAccess
    private val dpwsFramework: DpwsFramework
    private val sdcDevice: SdcDevice
    private var instanceIdentifier: InstanceIdentifier
    private var currentLocation: LocationDetail?

    init {
        injector = providerUtil.injector
        // bind to adapter matching ip
        logger.info { "Starting with address ${providerUtil.address}" }
        val networkInterface: NetworkInterface = NetworkInterface.getByInetAddress(
            InetAddress.getByName(providerUtil.address)
        )

        dpwsFramework = injector.getInstance(DpwsFramework::class.java)
        dpwsFramework.setNetworkInterface(networkInterface)
        mdibAccess = injector.getInstance(LocalMdibAccessFactory::class.java).createLocalMdibAccess()

        val handler = OperationHandler(mdibAccess)
        sdcDevice = injector.getInstance(SdcDeviceFactory::class.java)
            .createSdcDevice(
                object : DeviceSettings {
                    override fun getEndpointReference(): EndpointReferenceType {
                        return injector.getInstance(WsAddressingUtil::class.java)
                            .createEprWithAddress(providerUtil.epr)
                    }

                    override fun getNetworkInterface(): NetworkInterface {
                        return networkInterface
                    }
                }, mdibAccess, listOf<OperationInvocationReceiver>(handler), setOf<SdcDevicePlugin>(
                    injector.getInstance(
                        SdcRequiredTypesAndScopes::class.java
                    )
                )
            )
        instanceIdentifier = InstanceIdentifier()
        instanceIdentifier.rootName = "AwesomeExampleInstance"
        currentLocation = null
    }

    @Throws(Exception::class)
    override fun startUp() {
        val dpwsUtil = injector.getInstance(DpwsUtil::class.java)
        sdcDevice.hostingServiceAccess.setThisDevice(
            dpwsUtil.createDeviceBuilder()
                .setFriendlyName(
                    dpwsUtil.createLocalizedStrings()
                        .add("en", "Provider Example Unit")
                        .get()
                )
                .setFirmwareVersion("v1.2.3")
                .setSerialNumber("1234-5678-9101-1121").get()
        )
        sdcDevice.hostingServiceAccess.setThisModel(
            dpwsUtil.createModelBuilder()
                .setManufacturer(
                    dpwsUtil.createLocalizedStrings()
                        .add("en", "Provider Example Inc.")
                        .add("de", "Beispiel Provider AG")
                        .add("cn", "范例公司")
                        .get()
                )
                .setManufacturerUrl("http://www.example.com")
                .setModelName(
                    dpwsUtil.createLocalizedStrings()
                        .add("PEU")
                        .get()
                )
                .setModelNumber("54-32-1")
                .setPresentationUrl("http://www.example.com")
                .get()
        )
        val modificationsBuilderFactory = injector.getInstance(
            ModificationsBuilderFactory::class.java
        )

        // load initial mdib from file
        val mdibXmlIo = injector.getInstance(MdibXmlIo::class.java)
        val mdibAsStream = Provider::class.java.classLoader.getResourceAsStream("mdib.xml")
            ?: throw RuntimeException("Could not load mdib.xml as resource")
        val mdib = mdibXmlIo.readMdib(mdibAsStream)
        val modifications = modificationsBuilderFactory.createModificationsBuilder(mdib).get()
        mdibAccess.writeDescription(modifications)
        if (currentLocation != null) {
            // update the location again to match mdib and scopes
            setLocation(currentLocation!!)
        }

        val funnyExtension = ExtensionType()
        funnyExtension.any.add(
            org.somda.sdc.extension.model.codedattribute.ObjectFactory().createCodedAttributes(
                CodedAttributesType.builder()
                    .addCodedStringAttribute(
                        CodedStringAttributeType.builder().withMdcAttribute(
                            MdcAttributeType.builder()
                                .withCode("Whats")
                                .withCodingSystem("Up")
                                .withSymbolicCodeName("Dog")
                                .build()
                        ).withValue("Nothing much how about you?").build()
                    ).withMustUnderstand(true)
                    .build()
            )
        )

        val mdsDescriptor = mdibAccess.findEntitiesByType(MdsDescriptor::class.java).first()!!.getDescriptor(MdsDescriptor::class.java).orElseThrow()
        mdsDescriptor.extension = funnyExtension

        val extensionModifications = MdibDescriptionModifications.create()
        extensionModifications.add(MdibDescriptionModification.Type.UPDATE, mdsDescriptor)

        mdibAccess.writeDescription(extensionModifications)
        dpwsFramework.startAsync().awaitRunning()
        sdcDevice.startAsync().awaitRunning()
    }

    override fun shutDown() {
        sdcDevice.stopAsync().awaitTerminated()
        dpwsFramework.stopAsync().awaitTerminated()
        sdcDevice.stopAsync().awaitTerminated()
    }

    @Throws(PreprocessingException::class)
    fun setLocation(location: LocationDetail) {
        val newInstanceIdentifier = FallbackInstanceIdentifier.create(location)
        newInstanceIdentifier.ifPresent { ii: InstanceIdentifier ->
            instanceIdentifier = ii
            logger.info { "Updated instanceIdentifier to ${instanceIdentifier.rootName}" }
        }
        if (this.isRunning || state() === Service.State.STARTING) {
            logger.info("Updating location context")
            val locMod = MdibStateModifications.create(MdibStateModifications.Type.CONTEXT)
            mdibAccess.startTransaction().use { readTransaction ->
                val locDesc = readTransaction.getDescriptor(
                    Handles.HANDLE_LOCATIONCONTEXT,
                    LocationContextDescriptor::class.java
                ).orElseThrow {
                    RuntimeException(
                        java.lang.String.format(
                            "Could not find state for handle %s",
                            Handles.HANDLE_LOCATIONCONTEXT
                        )
                    )
                }
                val locState = LocationContextState()
                locState.locationDetail = location
                locState.descriptorVersion = locDesc.descriptorVersion
                locState.descriptorHandle = locDesc.handle
                locState.stateVersion = BigInteger.ONE
                locState.handle = locDesc.handle + "State"
                locState.bindingMdibVersion = mdibAccess.mdibVersion.version
                locState.contextAssociation = ContextAssociation.ASSOC
                locState.validator.add(instanceIdentifier)
                locState.identification.add(instanceIdentifier)
                locMod.add(locState)
            }
            mdibAccess.writeStates(locMod)
        }
        currentLocation = location.clone() as LocationDetail
    }

    /**
     * Adds a sine wave to the data of a waveform.
     *
     * @param handle descriptor handle of waveform state
     * @throws PreprocessingException if changes could not be committed to mdib
     */
    @Throws(PreprocessingException::class)
    fun changeWaveform(handle: String) {
        val modifications = MdibStateModifications.create(MdibStateModifications.Type.WAVEFORM)
        mdibAccess.startTransaction().use { readTransaction ->
            val state = readTransaction.getState(
                handle,
                RealTimeSampleArrayMetricState::class.java
            ).orElseThrow {
                RuntimeException(
                    String.format("Could not find state for handle %s", handle)
                )
            }
            val metricQuality = MetricQuality()
            metricQuality.mode = GenerationMode.REAL
            metricQuality.validity = MeasurementValidity.VLD
            val sampleArrayValue = SampleArrayValue()
            sampleArrayValue.metricQuality = metricQuality
            val minValue = 0
            val maxValue = 50
            val sampleCapacity = 10

            // sine wave
            val values = LinkedList<BigDecimal>()
            val delta = 2 * Math.PI / sampleCapacity
            IntStream.range(0, sampleCapacity)
                .forEachOrdered { n: Int ->
                    values.add(
                        BigDecimal.valueOf((Math.sin(n * delta) + 1) / 2.0 * (maxValue - minValue) + minValue)
                            .setScale(15, RoundingMode.DOWN)
                    )
                }
            sampleArrayValue.samples = values
            sampleArrayValue.determinationTime = Instant.now()
            state.metricValue = sampleArrayValue
            modifications.add(state)
        }
        mdibAccess.writeStates(modifications)
    }

    /**
     * Increments the value of a NumericMetricState.
     *
     * @param handle descriptor handle of metric state
     * @throws PreprocessingException if changes could not be committed to mdib
     */
    @Throws(PreprocessingException::class)
    fun changeNumericMetric(handle: String) {
        val stateOpt = mdibAccess.getState(
            handle,
            NumericMetricState::class.java
        )
        val state = stateOpt.get()
        var metricValue = state.metricValue
        if (metricValue != null && metricValue.value != null) {
            metricValue.value = metricValue.value.add(BigDecimal.ONE)
        } else {
            metricValue = NumericMetricValue()
            metricValue.value = BigDecimal.ONE
        }
        metricValue.determinationTime = Instant.now()
        BaseUtil.addMetricQualityDemo(metricValue)
        state.metricValue = metricValue
        mdibAccess.writeStates(MdibStateModifications.create(MdibStateModifications.Type.METRIC).add(state))
    }

    /**
     * Changes the content of a StringMetricState, toggling between "UPPERCASE" and "lowercase" as content.
     *
     * @param handle descriptor handle of metric state
     * @throws PreprocessingException if changes could not be committed to mdib
     */
    @Throws(PreprocessingException::class)
    fun changeStringMetric(handle: String) {
        val stateOpt = mdibAccess.getState(
            handle,
            StringMetricState::class.java
        )
        val state = stateOpt.get()
        var metricValue = state.metricValue
        if (metricValue != null && metricValue.value != null) {
            val actVal = metricValue.value
            if (actVal == "UPPERCASE") {
                metricValue.value = "lowercase"
            } else {
                metricValue.value = "UPPERCASE"
            }
        } else {
            metricValue = StringMetricValue()
            metricValue.value = "initial VALUE"
        }
        metricValue.determinationTime = Instant.now()
        BaseUtil.addMetricQualityDemo(metricValue)
        state.metricValue = metricValue
        mdibAccess.writeStates(MdibStateModifications.create(MdibStateModifications.Type.METRIC).add(state))
    }

    /**
     * Changes the content of an EnumStringMetricState, selecting the next allowed value.
     *
     * @param handle descriptor handle of metric state
     * @throws PreprocessingException if changes could not be committed to mdib
     */
    @Throws(PreprocessingException::class)
    fun changeEnumStringMetric(handle: String) {
        val entityOpt = mdibAccess.getEntity(handle)
        val mdibEntity = entityOpt.get()
        val descriptor = mdibEntity.descriptor as EnumStringMetricDescriptor
        val allowedValue =
            descriptor.allowedValue.stream().map { x: AllowedValue -> x.value }.collect(Collectors.toList())
        val state = mdibEntity.states[0] as EnumStringMetricState
        var metricValue = state.metricValue
        if (metricValue != null && metricValue.value != null) {
            val actVal = metricValue.value
            val iter: Iterator<String> = Iterables.cycle(allowedValue).iterator()
            var next = iter.next()
            // since Iterables.cycle by definition creates an infinite iterator,
            // having a break condition is a safety mechanism
            // this will either select the *next* allowed value or stop at some value
            // It's by no means a good idea for a production solution, but kinda
            // neat for this example.
            var i = 0
            while (iter.hasNext() && i < MAX_ENUM_ITERATIONS) {
                if (next == actVal) {
                    next = iter.next()
                    break
                }
                next = iter.next()
                i++
            }
            metricValue.value = next
        } else {
            metricValue = StringMetricValue()
            metricValue.value = allowedValue[0]
        }
        metricValue.determinationTime = Instant.now()
        BaseUtil.addMetricQualityDemo(metricValue)
        state.metricValue = metricValue
        mdibAccess.writeStates(MdibStateModifications.create(MdibStateModifications.Type.METRIC).add(state))
    }

    /**
     * Toggles an AlertSignalState and AlertConditionState between presence on and off.
     *
     * @param signalHandle    signal handle to toggle
     * @param conditionHandle matching condition to toggle
     */
    fun changeAlertSignalAndConditionPresence(signalHandle: String, conditionHandle: String) {
        val signalEntity = mdibAccess.getEntity(signalHandle).get()
        val conditionEntity = mdibAccess.getEntity(conditionHandle).get()
        val signalState = signalEntity.states[0] as AlertSignalState
        val conditionState = conditionEntity.states[0] as AlertConditionState
        if (signalState.presence == null || signalState.presence == AlertSignalPresence.ON) {
            signalState.presence = AlertSignalPresence.OFF
            conditionState.isPresence = false
        } else {
            signalState.presence = AlertSignalPresence.ON
            conditionState.isPresence = true
        }
        try {
            mdibAccess.writeStates(
                MdibStateModifications
                    .create(MdibStateModifications.Type.ALERT).add(conditionState).add(signalState)
            )
        } catch (e: PreprocessingException) {
            logger.error(e)
        }
    }

    companion object: Logging {
        private const val MAX_ENUM_ITERATIONS = 17
        @Throws(IOException::class, PreprocessingException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val util = ProviderUtil()
            util.main(args)
            val targetFacility = System.getenv().getOrDefault("ref_fac", Config.DEFAULT_FACILITY)
            val targetBed = System.getenv().getOrDefault("ref_bed", Config.DEFAULT_BED)
            val targetPoC = System.getenv().getOrDefault("ref_poc", Config.DEFAULT_POC)
            val provider = Provider(util)

            // set a location for scopes
            val loc = LocationDetail()
            loc.bed = targetBed
            loc.poC = targetPoC
            loc.facility = targetFacility
            provider.setLocation(loc)
            provider.startAsync().awaitRunning()

            // generate some data
            logger.info { "Sending waveforms every {${util.waveformInterval.toMillis()}}ms" }
            val t1 = Thread {
                while (true) {
                    try {
                        Thread.sleep(util.waveformInterval.toMillis())
                        provider.changeWaveform(Handles.HANDLE_WAVEFORM)
                    } catch (e: Exception) {
                        logger.warn(e) { "Thread loop stopping" }
                        break
                    }
                }
            }
            t1.isDaemon = true
            t1.start()
            logger.info { "Sending reports every ${util.reportInterval.toMillis()}ms" }
            val t2 = Thread {
                while (true) {
                    try {
                        Thread.sleep(util.reportInterval.toMillis())
                        provider.changeNumericMetric(Handles.HANDLE_NUMERIC_DYNAMIC)
                        provider.changeStringMetric(Handles.HANDLE_STRING_DYNAMIC)
                        provider.changeEnumStringMetric(Handles.HANDLE_ENUM_DYNAMIC)
                        provider.changeAlertSignalAndConditionPresence(
                            Handles.HANDLE_ALERT_SIGNAL, Handles.HANDLE_ALERT_CONDITION
                        )
                    } catch (e: InterruptedException) {
                        logger.warn("Thread loop stopping", e)
                        break
                    } catch (e: PreprocessingException) {
                        logger.warn("Thread loop stopping", e)
                        break
                    }
                }
            }
            t2.isDaemon = true
            t2.start()

            // graceful shutdown using sigterm
            Runtime.getRuntime().addShutdownHook(Thread {
                t1.interrupt()
                t2.interrupt()
                provider.stopAsync().awaitTerminated()
            })
            try {
                System.`in`.read()
            } catch (e: IOException) {
                // pass and quit
            }
            t1.interrupt()
            t2.interrupt()
            provider.stopAsync().awaitTerminated()
        }
    }
}
