package org.somda.sdc.taint.provider

import org.apache.logging.log4j.LogManager
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.PreprocessingException
import org.somda.sdc.biceps.model.message.InvocationError
import org.somda.sdc.biceps.model.message.InvocationState
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor.AllowedValue
import org.somda.sdc.biceps.provider.access.LocalMdibAccess
import org.somda.sdc.glue.provider.sco.Context
import org.somda.sdc.glue.provider.sco.IncomingSetServiceRequest
import org.somda.sdc.glue.provider.sco.InvocationResponse
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver
import org.somda.sdc.taint.BaseUtil
import org.somda.sdc.taint.Handles
import java.math.BigDecimal
import java.time.Instant
import java.util.function.Consumer


/**
 * This class provides a handler for incoming operations on the sdc provider.
 *
 *
 * It implements generic handlers for some operations, which enables handling operations easily, although
 * a real application should be a little more specialized in its handling.
 */
class OperationHandler(private val mdibAccess: LocalMdibAccess) : OperationInvocationReceiver {
    @JvmOverloads
    fun createLocalizedText(text: String?, lang: String? = "en"): LocalizedText {
        val localizedText = LocalizedText()
        localizedText.value = text
        localizedText.lang = lang
        return localizedText
    }

    fun genericSetValue(context: Context, data: BigDecimal?): InvocationResponse {
        // TODO: Check if state is modifiable
        context.sendSuccessfulReport(InvocationState.START)
        val operationHandle = context.operationHandle
        LOG.debug("Received SetValue request for {}: {}", operationHandle, data)

        // find operation target
        val setNumeric = mdibAccess.getDescriptor(
            operationHandle,
            SetValueOperationDescriptor::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target cannot be found")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL, InvocationError.OTH, java.util.List.of(errorMessage)
            )
            throw RuntimeException(String.format("Operation descriptor %s missing", operationHandle))
        }
        val operationTargetHandle = setNumeric.operationTarget
        val targetDesc = mdibAccess.getDescriptor(
            operationTargetHandle,
            NumericMetricDescriptor::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target cannot be found")
            context.sendUnsuccessfulReport(InvocationState.FAIL, InvocationError.OTH, listOf(errorMessage))
            throw RuntimeException(
                String.format(
                    "Operation target descriptor %s missing",
                    operationTargetHandle
                )
            )
        }

        // find allowed range for descriptor and verify it's within
        targetDesc.technicalRange.forEach(
            Consumer { range: Range ->
                if (range.lower != null && range.lower.compareTo(data) > 0) {
                    // value too small
                    val errorMessage = createLocalizedText("Value too small")
                    context.sendUnsuccessfulReport(InvocationState.FAIL, InvocationError.OTH, listOf(errorMessage))
                    throw RuntimeException(
                        String.format(
                            "Operation set value below lower limit of %s, was %s",
                            range.lower, data
                        )
                    )
                }
                if (range.upper != null && range.upper.compareTo(data) < 0) {
                    // value too big
                    val errorMessage = createLocalizedText("Value too big")
                    context.sendUnsuccessfulReport(InvocationState.FAIL, InvocationError.OTH, listOf(errorMessage))
                    throw RuntimeException(
                        String.format(
                            "Operation set value below lower limit of %s, was %s",
                            range.lower, data
                        )
                    )
                }
            }
        )
        val targetState = mdibAccess.getState(
            operationTargetHandle,
            NumericMetricState::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target state cannot be found")
            context.sendUnsuccessfulReport(InvocationState.FAIL, InvocationError.OTH, listOf(errorMessage))
            throw RuntimeException(
                String.format(
                    "Operation target descriptor %s missing",
                    operationTargetHandle
                )
            )
        }
        if (targetState.metricValue == null) {
            targetState.metricValue = NumericMetricValue()
        }
        targetState.metricValue.value = data
        targetState.metricValue.determinationTime = Instant.now()
        BaseUtil.addMetricQualityDemo(targetState.metricValue)
        val mod = MdibStateModifications.create(MdibStateModifications.Type.METRIC)
        mod.add(targetState)
        return try {
            mdibAccess.writeStates(mod)
            context.sendSuccessfulReport(InvocationState.FIN)
            context.createSuccessfulResponse(InvocationState.FIN)
        } catch (e: PreprocessingException) {
            LOG.error("Error while writing states", e)
            val errorMessage = createLocalizedText("Error while writing states")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL,
                InvocationError.UNSPEC,
                java.util.List.of(errorMessage)
            )
            context.createUnsuccessfulResponse(
                InvocationState.FAIL, InvocationError.UNSPEC, java.util.List.of(errorMessage)
            )
        }
    }

    fun genericSetString(context: Context, data: String, isEnumString: Boolean): InvocationResponse {
        // TODO: Check if state is modifiable
        context.sendSuccessfulReport(InvocationState.START)
        val operationHandle = context.operationHandle
        LOG.debug("Received SetString for {}: {}", operationHandle, data)

        // find operation target
        val setString = mdibAccess.getDescriptor(
            operationHandle,
            SetStringOperationDescriptor::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target cannot be found")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL, InvocationError.OTH, java.util.List.of(errorMessage)
            )
            throw RuntimeException(String.format("Operation descriptor %s missing", operationHandle))
        }
        val operationTargetHandle = setString.operationTarget

        // verify if new data is allowed for enum strings
        if (isEnumString) {
            val targetDesc = mdibAccess.getDescriptor(
                operationTargetHandle,
                EnumStringMetricDescriptor::class.java
            ).orElseThrow<RuntimeException> {
                val errorMessage = createLocalizedText("Operation target descriptor cannot be found")
                context.sendUnsuccessfulReport(
                    InvocationState.FAIL,
                    InvocationError.OTH,
                    java.util.List.of(errorMessage)
                )
                throw RuntimeException(
                    String.format(
                        "Operation target descriptor %s missing",
                        operationTargetHandle
                    )
                )
            }

            // validate data is allowed
            val first = targetDesc.allowedValue.stream().filter { x: AllowedValue -> x.value == data }.findFirst()
            if (first.isEmpty) {
                // not allowed value, bye bye
                val errormessage = createLocalizedText("Value is not allowed here")
                return context.createUnsuccessfulResponse(
                    mdibAccess.mdibVersion,
                    InvocationState.FAIL, InvocationError.UNSPEC, java.util.List.of(errormessage)
                )
            }
        }
        val targetState = mdibAccess.getState(
            operationTargetHandle,
            StringMetricState::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target state cannot be found")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL,
                InvocationError.OTH,
                java.util.List.of(errorMessage)
            )
            throw RuntimeException(
                String.format(
                    "Operation target descriptor %s missing",
                    operationTargetHandle
                )
            )
        }
        if (targetState.metricValue == null) {
            targetState.metricValue = StringMetricValue()
        }
        targetState.metricValue.value = data
        targetState.metricValue.determinationTime = Instant.now()
        BaseUtil.addMetricQualityDemo(targetState.metricValue)
        val mod = MdibStateModifications.create(MdibStateModifications.Type.METRIC)
        mod.add(targetState)
        return try {
            mdibAccess.writeStates(mod)
            context.sendSuccessfulReport(InvocationState.FIN)
            context.createSuccessfulResponse(InvocationState.FIN)
        } catch (e: PreprocessingException) {
            LOG.error("Error while writing states", e)
            val errorMessage = createLocalizedText("Error while writing states")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL,
                InvocationError.UNSPEC,
                java.util.List.of(errorMessage)
            )
            context.createUnsuccessfulResponse(
                InvocationState.FAIL,
                InvocationError.UNSPEC, java.util.List.of(errorMessage)
            )
        }
    }

    @IncomingSetServiceRequest(operationHandle = Handles.HANDLE_SET_VALUE)
    fun setSettableNumericMetric(context: Context, data: BigDecimal?): InvocationResponse {
        return genericSetValue(context, data)
    }

    @IncomingSetServiceRequest(operationHandle = Handles.HANDLE_SET_STRING)
    fun setSettableStringMetric(context: Context, data: String): InvocationResponse {
        return genericSetString(context, data, false)
    }

    @IncomingSetServiceRequest(operationHandle = Handles.HANDLE_SET_STRING_ENUM)
    fun setSettableEnumMetric(context: Context, data: String): InvocationResponse {
        return genericSetString(context, data, true)
    }

    @IncomingSetServiceRequest(operationHandle = Handles.HANDLE_ACTIVATE, listType = String::class)
    fun activateExample(context: Context, args: List<String?>?): InvocationResponse {
        context.sendSuccessfulReport(InvocationState.START)
        LOG.info("Received Activate for {}", Handles.HANDLE_ACTIVATE)
        context.sendSuccessfulReport(InvocationState.FIN)
        return context.createSuccessfulResponse(mdibAccess.mdibVersion, InvocationState.FIN)
    }

    @IncomingSetServiceRequest(operationHandle = "actop.mds0_sco_0", listType = String::class)
    fun activateExample2(context: Context, args: List<String?>?): InvocationResponse {
        context.sendSuccessfulReport(InvocationState.START)
        LOG.info("Received Activate for {}", context.operationHandle)
        context.sendSuccessfulReport(InvocationState.FIN)
        return context.createSuccessfulResponse(mdibAccess.mdibVersion, InvocationState.FIN)
    }

    companion object {
        private val LOG = LogManager.getLogger(
            OperationHandler::class.java
        )
    }
}
