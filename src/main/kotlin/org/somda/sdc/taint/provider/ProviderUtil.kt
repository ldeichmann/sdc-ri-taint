package org.somda.sdc.taint.provider

import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.defaultLazy
import com.github.ajalt.clikt.parameters.options.option
import com.google.inject.Guice
import com.google.inject.Injector
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.CommonConfig
import org.somda.sdc.biceps.common.CommonConstants
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.guice.DefaultBicepsModule
import org.somda.sdc.common.guice.DefaultCommonConfigModule
import org.somda.sdc.common.guice.DefaultCommonModule
import org.somda.sdc.dpws.DpwsConfig
import org.somda.sdc.dpws.crypto.CryptoConfig
import org.somda.sdc.dpws.crypto.CryptoSettings
import org.somda.sdc.dpws.guice.DefaultDpwsModule
import org.somda.sdc.dpws.soap.SoapConfig
import org.somda.sdc.glue.GlueConstants
import org.somda.sdc.glue.guice.DefaultGlueConfigModule
import org.somda.sdc.glue.guice.DefaultGlueModule
import org.somda.sdc.glue.guice.GlueDpwsConfigModule
import org.somda.sdc.taint.BaseUtil
import org.somda.sdc.taint.Config
import java.net.URI
import java.security.cert.X509Certificate
import java.time.Duration
import java.util.*
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

val DEFAULT_REPORT_INTERVAL: Duration = Duration.ofMillis(5000)
val DEFAULT_WAVEFORM_INTERVAL: Duration = Duration.ofMillis(100)
val SDC_SERVICE_CONSUMER_ROLE = URI.create(
    GlueConstants.OID_KEY_PURPOSE_SDC_SERVICE_CONSUMER
)

class ProviderUtil: BaseUtil() {
    val epr: String by option(help="The EPR")
        .defaultLazy(defaultForHelp = "random uuid") {
            "urn:uuid:${UUID.randomUUID()}"
        }

    val reportInterval: Duration by option(help="Interval in which to send reports in milliseconds")
        .convert { Duration.ofMillis(it.toLong()) }
        .default(DEFAULT_REPORT_INTERVAL)

    val waveformInterval: Duration by option(help="Interval in which to send waveforms in milliseconds")
        .convert { Duration.ofMillis(it.toLong()) }
        .default(DEFAULT_WAVEFORM_INTERVAL)

    val facility: String by option(envvar = "ref_fac").default(Config.DEFAULT_FACILITY)
    val bed: String by option(envvar = "ref_bed").default(Config.DEFAULT_BED)
    val poc: String by option(envvar = "ref_poc").default(Config.DEFAULT_POC)

    lateinit var injector: Injector

    companion object: Logging

    override fun run() {
        Configurator.reconfigure(localLoggerConfig(Level.INFO))

        injector = Guice.createInjector(
            DefaultCommonConfigModule(),
            DefaultGlueModule(),
            DefaultGlueConfigModule(),
            DefaultBicepsModule(),
            DefaultBicepsConfigModule(),
            DefaultCommonModule(),
            DefaultDpwsModule(),
            object : GlueDpwsConfigModule() {
                override fun customConfigure() {
                    // can't call this as we cannot override JAXB_CONTEXT_PATH otherwise
//                    super.customConfigure()
                    // add extension stuff
                    bind(
                        SoapConfig.JAXB_CONTEXT_PATH,
                        String::class.java,
                        CommonConstants.BICEPS_JAXB_CONTEXT_PATH + ":org.somda.sdc.extension.model.codedattribute");
                    bind(
                        SoapConfig.JAXB_SCHEMA_PATH,
                        String::class.java,
                        GlueConstants.SCHEMA_PATH + ":CodedAttribute.xsd"
                    )
                    bind(
                        SoapConfig.NAMESPACE_MAPPINGS,
                        String::class.java,
                        org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_MDPWS +
                                org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_BICEPS +
                                org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_GLUE
                    )
                    if (!disableTls) {
                        bind(
                            CryptoConfig.CRYPTO_SETTINGS,
                            CryptoSettings::class.java,
                            createCustomCryptoSettings()
                        )
                    }
                    bind(DpwsConfig.HTTPS_SUPPORT, Boolean::class.java, !disableTls)
                    bind(DpwsConfig.HTTP_SUPPORT, Boolean::class.java, disableTls)
                    bind(CryptoConfig.CRYPTO_DEVICE_HOSTNAME_VERIFIER,
                        HostnameVerifier::class.java,
                        HostnameVerifier bind@{ _: String?, session: SSLSession ->
                            try {
                                // since this is not a real implementation, we still want to allow all peers
                                // which is why this doesn't really filter anything
                                // returning false in this filter would reject an incoming request
                                val peerCerts = session.peerCertificates
                                val x509 = peerCerts[0] as X509Certificate
                                val extendedKeyUsage = x509.extendedKeyUsage
                                if (extendedKeyUsage == null || extendedKeyUsage.isEmpty()) {
                                    logger.warn { "No EKU in peer certificate" }
                                    return@bind true
                                }

                                // find matching provider key purpose
                                for (key in extendedKeyUsage) {
                                    try {
                                        val keyUri = URI.create(key)
                                        if (keyUri == SDC_SERVICE_CONSUMER_ROLE) {
                                            logger.debug { "SDC Service Consumer PKP found" }
                                            return@bind true
                                        }
                                    } catch (e: IllegalArgumentException) {
                                        // don't care, was no uri
                                    }
                                }
                                return@bind true
                            } catch (e: Exception) {
                                logger.error(e) { "Error while validating client certificate" }
                                logger.trace(e) { "Error while validating client certificate" }
                            }
                            false
                        })
                }
            })
    }
}