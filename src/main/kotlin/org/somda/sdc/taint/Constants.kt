package org.somda.sdc.taint

import org.somda.sdc.biceps.model.participant.CodedValue

object Handles {
    const val HANDLE_LOCATIONCONTEXT = "LC.mds0"
    const val HANDLE_PATIENTCONTEXT = "PC.mds0"
    const val HANDLE_ALERT_SIGNAL = "as0.mds0"
    const val HANDLE_ALERT_CONDITION = "ac0.mds0"
    const val HANDLE_NUMERIC_DYNAMIC = "numeric.ch1.vmd0"
    const val HANDLE_ENUM_DYNAMIC = "enumstring2.ch0.vmd0"
    const val HANDLE_STRING_DYNAMIC = "string2.ch0.vmd1"
    const val HANDLE_WAVEFORM = "rtsa.ch0.vmd0"

    const val HANDLE_ACTIVATE = "actop.vmd1_sco_0"
    const val HANDLE_SET_VALUE = "numeric.ch0.vmd1_sco_0"
    const val HANDLE_SET_STRING_ENUM = "enumstring.ch0.vmd1_sco_0"
    const val HANDLE_SET_STRING = "string.ch0.vmd1_sco_0"

    const val HANDLE_NUMERIC_SETTABLE = "numeric.ch0.vmd1"
    const val HANDLE_ENUM_SETTABLE = "enumstring.ch0.vmd1"
    const val HANDLE_STRING_SETTABLE = "string.ch0.vmd1"
}

object Config {
    const val DEFAULT_IP = "127.0.0.1"
    const val DEFAULT_FACILITY = "r_fac"
    const val DEFAULT_BED = "r_bed"
    const val DEFAULT_POC = "r_poc"
    const val DEFAULT_REPORT_TIMEOUT = "30"
}
object Codes {
    val HANDLE_ACTIVATE_CODE = CodedValue()
    init {
        HANDLE_ACTIVATE_CODE.code = "196279"
        HANDLE_ACTIVATE_CODE.codingSystem = "urn:oid:1.2.840.10004.1.1.1.0.0.1"
    }

    val HANDLE_SET_VALUE_CODE = CodedValue()

    init {
        HANDLE_SET_VALUE_CODE.code = "196276"
        HANDLE_SET_VALUE_CODE.codingSystem = "urn:oid:1.2.840.10004.1.1.1.0.0.1"
    }

    val HANDLE_SET_STRING_ENUM_CODE = CodedValue()

    init {
        HANDLE_SET_STRING_ENUM_CODE.code = "196277"
        HANDLE_SET_STRING_ENUM_CODE.codingSystem = "urn:oid:1.2.840.10004.1.1.1.0.0.1"
    }


    val HANDLE_SET_STRING_CODE = CodedValue()

    init {
        HANDLE_SET_STRING_CODE.code = "196278"
        HANDLE_SET_STRING_CODE.codingSystem = "urn:oid:1.2.840.10004.1.1.1.0.0.1"
    }
}