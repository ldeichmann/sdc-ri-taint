package org.somda.sdc.taint

import org.somda.sdc.extension.model.codedattribute.MdcAttributeType

class ExampleExtendedProvider


fun main() {
    println("Very cool")

    val mdcAttributeType = MdcAttributeType()
    mdcAttributeType.code = "abc"

    println("also cool: $mdcAttributeType")
}