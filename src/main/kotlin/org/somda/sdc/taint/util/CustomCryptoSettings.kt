package org.somda.sdc.taint.util

import org.apache.logging.log4j.kotlin.Logging
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder
import org.bouncycastle.operator.InputDecryptorProvider
import org.bouncycastle.operator.OperatorCreationException
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo
import org.bouncycastle.pkcs.PKCSException
import org.somda.sdc.dpws.crypto.CryptoSettings
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.security.*
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.util.*

data class CustomCryptoSettings(
    val keyStore: List<Byte>,
    val trustStore: List<Byte>,
    val keyStorePasswd: String,
    val trustStorePasswd: String,
) : CryptoSettings {

    override fun getKeyStoreStream(): Optional<InputStream> {
        return Optional.of(ByteArrayInputStream(keyStore.toByteArray()))
    }

    override fun getKeyStorePassword(): String {
        return keyStorePasswd
    }

    override fun getTrustStoreStream(): Optional<InputStream> {
        return Optional.of(ByteArrayInputStream(trustStore.toByteArray()))
    }

    override fun getTrustStorePassword(): String {
        return trustStorePasswd
    }

    companion object: Logging {

        fun fromKeyStore(
            keyStorePath: String,
            trustStorePath: String,
            keyStorePassword: String,
            trustStorePassword: String
        ): CustomCryptoSettings {
            val keyStoreFile: List<Byte>
            val trustStoreFile: List<Byte>
            try {
                keyStoreFile = Files.readAllBytes(Path.of(keyStorePath)).toList()
                trustStoreFile = Files.readAllBytes(Path.of(trustStorePath)).toList()
            } catch (e: IOException) {
                logger.error("Specified store file could not be found", e)
                throw RuntimeException("Specified store file could not be found", e)
            }
            return CustomCryptoSettings(keyStoreFile, trustStoreFile, keyStorePassword, trustStorePassword)
        }

        fun fromKeyFile(
            userKeyFilePath: String,
            userCertFilePath: String,
            caCertFilePath: String,
            userKeyPassword: String
        ): CustomCryptoSettings {
            Security.addProvider(BouncyCastleProvider())
            val userKeyFile: ByteArray
            val userCertFile: ByteArray
            val caCertFile: ByteArray
            try {
                userKeyFile = Files.readAllBytes(Path.of(userKeyFilePath))
                userCertFile = Files.readAllBytes(Path.of(userCertFilePath))
                caCertFile = Files.readAllBytes(Path.of(caCertFilePath))
            } catch (e: IOException) {
                logger.error("Specified certificate file could not be found", e)
                throw RuntimeException("Specified certificate file could not be found", e)
            }
            val userKey: PrivateKey
            val userCert: Certificate
            val caCert: Certificate
            try {
                val kf = KeyFactory.getInstance("RSA")
                val cf = CertificateFactory.getInstance("X.509")

                // private key
                userKey = getPrivateKey(userKeyFile, userKeyPassword)

                // public key
                userCert = cf.generateCertificate(ByteArrayInputStream(userCertFile))

                // ca cert
                caCert = cf.generateCertificate(ByteArrayInputStream(caCertFile))
            } catch (e: NoSuchAlgorithmException) {
                logger.error("Specified certificate file could not be loaded", e)
                throw RuntimeException("Specified certificate file could not be loaded", e)
            } catch (e: CertificateException) {
                logger.error("Specified certificate file could not be loaded", e)
                throw RuntimeException("Specified certificate file could not be loaded", e)
            } catch (e: IOException) {
                logger.error("Specified certificate file could not be loaded", e)
                throw RuntimeException("Specified certificate file could not be loaded", e)
            }
            val keyStore: KeyStore
            val trustStore: KeyStore
            try {
                keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
                keyStore.load(null)
                trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
                trustStore.load(null)
            } catch (e: CertificateException) {
                logger.error("Error creating keystore instance", e)
                throw RuntimeException("Error creating keystore instance", e)
            } catch (e: NoSuchAlgorithmException) {
                logger.error("Error creating keystore instance", e)
                throw RuntimeException("Error creating keystore instance", e)
            } catch (e: IOException) {
                logger.error("Error creating keystore instance", e)
                throw RuntimeException("Error creating keystore instance", e)
            } catch (e: KeyStoreException) {
                logger.error("Error creating keystore instance", e)
                throw RuntimeException("Error creating keystore instance", e)
            }
            try {
                keyStore.setKeyEntry("key", userKey, userKeyPassword.toCharArray(), arrayOf(userCert))
                trustStore.setCertificateEntry("ca", caCert)
            } catch (e: KeyStoreException) {
                logger.error("Error loading certificate into keystore instance", e)
                throw RuntimeException("Error loading certificate into keystore instance", e)
            }
            val keyStoreOutputStream = ByteArrayOutputStream()
            val trustStoreOutputStream = ByteArrayOutputStream()
            try {
                keyStore.store(keyStoreOutputStream, userKeyPassword.toCharArray())
                trustStore.store(trustStoreOutputStream, userKeyPassword.toCharArray())
            } catch (e: KeyStoreException) {
                logger.error("Error converting keystore to stream", e)
                throw RuntimeException("Error converting keystore to stream", e)
            } catch (e: IOException) {
                logger.error("Error converting keystore to stream", e)
                throw RuntimeException("Error converting keystore to stream", e)
            } catch (e: NoSuchAlgorithmException) {
                logger.error("Error converting keystore to stream", e)
                throw RuntimeException("Error converting keystore to stream", e)
            } catch (e: CertificateException) {
                logger.error("Error converting keystore to stream", e)
                throw RuntimeException("Error converting keystore to stream", e)
            }
            return CustomCryptoSettings(
                keyStoreOutputStream.toByteArray().toList(),
                trustStoreOutputStream.toByteArray().toList(),
                userKeyPassword,
                userKeyPassword
            )
        }

        @Throws(IOException::class)
        private fun getPrivateKey(key: ByteArray, password: String): PrivateKey {
            val pp = PEMParser(BufferedReader(InputStreamReader(ByteArrayInputStream(key))))
            val pemKey = pp.readObject() as PKCS8EncryptedPrivateKeyInfo
            pp.close()
            val pkcs8Prov: InputDecryptorProvider = try {
                JceOpenSSLPKCS8DecryptorProviderBuilder().build(password.toCharArray())
            } catch (e: OperatorCreationException) {
                throw IOException(e)
            }
            val converter = JcaPEMKeyConverter().setProvider(BouncyCastleProvider())
            val decrypted: PrivateKeyInfo = try {
                pemKey.decryptPrivateKeyInfo(pkcs8Prov)
            } catch (e: PKCSException) {
                throw IOException(e)
            }
            return converter.getPrivateKey(decrypted)
        }
    }
}
