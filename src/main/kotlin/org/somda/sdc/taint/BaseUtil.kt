package org.somda.sdc.taint

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.file
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.model.participant.AbstractMetricValue
import org.somda.sdc.biceps.model.participant.AbstractMetricValue.MetricQuality
import org.somda.sdc.biceps.model.participant.GenerationMode
import org.somda.sdc.biceps.model.participant.MeasurementValidity
import org.somda.sdc.common.logging.InstanceLogger
import org.somda.sdc.dpws.crypto.CryptoSettings
import org.somda.sdc.glue.consumer.helper.HostingServiceLogger
import org.somda.sdc.taint.util.CustomCryptoSettings
import java.io.File
import java.util.function.Consumer

abstract class BaseUtil: CliktCommand() {

    val address: String by option(help="address to bind to").required()

    val disableTls: Boolean by option(help = "Disable tls").flag(default = false)
    private val publicKey: File? by option(help = "Location of the public key").file(mustExist = true)
    private val privateKey: File? by option(help = "Location of the private key").file(mustExist = true)
    private val caCert: File? by option(help = "Location of the ca certificate").file(mustExist = true)
    private val privateKeyPassword: String? by option(help = "Password for the private key")



    companion object: Logging {
        private val CHATTY_LOGGERS = listOf(
            "org.apache.http.wire",
            "org.apache.http.headers",
            "org.eclipse.jetty"
//            "org.somda.sdc.biceps.provider.access.LocalMdibAccessImpl",
//            "org.somda.sdc.biceps.common.storage.MdibStorageImpl"
        )

        private const val CUSTOM_PATTERN = ("%d{HH:mm:ss.SSS}"
                + " [%thread]" // only include the space if we have a variable for these
                + " %notEmpty{[%X{" + InstanceLogger.INSTANCE_ID + "}] }"
                + " %notEmpty{[%X{" + HostingServiceLogger.HOSTING_SERVICE_INFO + "}] }"
                + "%-5level"
                + " %logger{36}"
                + " - %msg%n")

        fun addMetricQualityDemo(metricValue: AbstractMetricValue) {
            if (metricValue.metricQuality == null) {
                val qual = MetricQuality()
                qual.mode = GenerationMode.DEMO
                qual.validity = MeasurementValidity.VLD
                metricValue.metricQuality = qual
            }
        }

    }

    open fun createCustomCryptoSettings(): CryptoSettings {
        // certificates method
        // check that everything is set
        checkNotNull(this.publicKey)
        checkNotNull(this.privateKey)
        checkNotNull(this.caCert)
        checkNotNull(this.privateKeyPassword)

        return CustomCryptoSettings.fromKeyFile(
            privateKey!!.path, publicKey!!.path, caCert!!.path, privateKeyPassword!!
        )
    }


    protected open fun localLoggerConfig(consoleLevel: Level?): BuiltConfiguration? {
        val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
        builder.setStatusLevel(Level.ERROR)
        builder.setConfigurationName("LocalLogging")
        val layoutBuilder = builder
            .newLayout("PatternLayout")
            .addAttribute("pattern", CUSTOM_PATTERN)
        val rootLogger = builder.newRootLogger(Level.DEBUG)
        run {

            // create a console appender
            val appenderBuilder = builder
                .newAppender("console_logger", ConsoleAppender.PLUGIN_NAME)
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
            appenderBuilder.add(layoutBuilder)
            // only log WARN or worse to console
            appenderBuilder.addComponent(
                builder.newFilter(
                    "ThresholdFilter",
                    Filter.Result.ACCEPT,
                    Filter.Result.DENY
                )
                    .addAttribute("level", consoleLevel)
            )
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        run {
            // quiet down chatty loggers
            CHATTY_LOGGERS.forEach(Consumer { logger: String? ->
                builder.add(
                    builder.newLogger(logger, Level.INFO)
                        .addAttribute("additivity", true)
                )
            })
        }
        builder.add(rootLogger)
        return builder.build()
    }

}