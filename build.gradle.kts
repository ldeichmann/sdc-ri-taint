
plugins {
    kotlin("jvm") version "1.6.20"
}

group = "org.somda"
version = "1.0-SNAPSHOT"

val sdcRiVersion = "2.1.0-SNAPSHOT"

repositories {
    mavenCentral()
    mavenLocal()
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
        mavenContent {
            snapshotsOnly()
        }
    }
}

val extensionModel = "extension-model/"

val compileMavenProject = tasks.withType<Exec> {
    workingDir = file(extensionModel)
    commandLine = listOf("/usr/bin/mvn", "clean", "compile")
}

val downloadMavenDependencies = tasks.withType<Exec> {
    workingDir =  file(extensionModel)
    commandLine = listOf("/usr/bin/mvn", "dependency:copy-dependencies", "-DoutputDirectory=target/libs")
}


dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation(files("$extensionModel/target/classes").builtBy(compileMavenProject))
    implementation(files("$extensionModel/target/libs").builtBy(downloadMavenDependencies))

    implementation("org.somda.sdc:common:$sdcRiVersion")
    implementation("org.somda.sdc:biceps:$sdcRiVersion")
    implementation("org.somda.sdc:biceps-model:$sdcRiVersion")
    implementation("org.somda.sdc:dpws:$sdcRiVersion")
    implementation("org.somda.sdc:glue:$sdcRiVersion")

    implementation("com.github.ajalt.clikt:clikt:3.4.2")

    implementation("org.bouncycastle:bcprov-jdk15on:1.70")
    implementation("org.bouncycastle:bcpkix-jdk15on:1.70")

    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api-kotlin
    implementation(group = "org.apache.logging.log4j", name = "log4j-api-kotlin", version = "1.1.0")

    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
    implementation(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.17.2")

//    implementation(group = "org.apache.logging.log4j", name = "log4j-slf4j-impl", version = "2.17.2")
    implementation(group = "org.apache.logging.log4j", name = "log4j-slf4j18-impl", version = "2.17.2")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}